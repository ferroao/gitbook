Plotting monocentric chromosomes
================

This guide shows the files to plot idiograms of measured karyotypes and
optionally marks.

## 0\. Load package

visit gitlab for installation instructions
<https://gitlab.com/ferroao/idiogramFISH>

``` r
#load package
library(idiogramFISH) 
```

## 1\. Get your chromosome size data

Initially you have to open your chromosome data as a
dataframe.

### From scratch:

``` r
# Example dataframe written in R, use: (column OTU is optional if only 1 OTU)
mydfChrSize<-read.table(text=
"            OTU chrName shortArmSize longArmSize
1 \"Species one\"   1     1.5         2.0
2 \"Species one\"   2     2.0         2.5
3 \"Species one\"   3     1.0         1.5
4 \"Species one\"   X     2.0         3.5"  ,  header=TRUE, stringsAsFactors=FALSE,fill=TRUE)

# just to show it here
knitr::kable(mydfChrSize)
```

| OTU         | chrName | shortArmSize | longArmSize |
| :---------- | :------ | -----------: | ----------: |
| Species one | 1       |          1.5 |         2.0 |
| Species one | 2       |          2.0 |         2.5 |
| Species one | 3       |          1.0 |         1.5 |
| Species one | X       |          2.0 |         3.5 |

### or loading saved data:

Initially, if you use RStudio, use menu “Session”, “Set working
directory” for choosing your desired folder or:

    setwd("~/folder/subfolder")

Open your chromosome data dataframe importing it from a .csv (read.csv)
or .xls file (readxl).

    mydfChrSize<-read.csv("somefile.csv")

For fixing column names
    use:

    colnames(mydfChrSize)<-c("OTU", "chrName","shortArmSize","longArmSize")

## 2\. Get marks general data

Open or make your mark data as a dataframe. This dataframe has the marks
present in all karyotypes without position info.

<style>
.verysmall td {line-height: 15px; vertical-align: bottom; 
    font-size:10pt;
    padding-top:3px;
    padding-bottom:0px;
    horizontal-align: center;
    }
</style>

``` r
# From scratch:
mydfMarkColor<-read.table(text=
"  markName markColor  style
1       5S       red   dots
2      45S     green square
3     DAPI      blue square
4      CMA    yellow square"  ,  header=TRUE, stringsAsFactors=FALSE,fill=TRUE)
```

<div class="verysmall">

``` r
mydfMarkColor
```

<div class="kable-table">

| markName | markColor | style  |
| :------- | :-------- | :----- |
| 5S       | red       | dots   |
| 45S      | green     | square |
| DAPI     | blue      | square |
| CMA      | yellow    | square |

</div>

</div>

For fixing column names
    use:

    colnames(mydfMarkColor)<-c("markName", "markColor","style") # if style column not present it will be filled with "square"

## 3\. Get marks positions data

Open or write your mark positions as a dataframe. This dataframe has the
marks present in all karyotypes with position info.

``` r
# We will use column OTU if dataframe because chromosome size df has it
mydfOfMarks<-read.table(text=
"            OTU chrName markName markArm markSize markDistCen
1 \"Species one\"      1       5S       p      0.5         0.5
2 \"Species one\"      1      45S       q        1         0.5
3 \"Species one\"      X      45S       p        1         1.0
4 \"Species one\"      3     DAPI       q        1         1.0"  ,  header=TRUE, stringsAsFactors=FALSE,fill=TRUE)

knitr::kable(mydfOfMarks)
```

| OTU         | chrName | markName | markArm | markSize | markDistCen |
| :---------- | :------ | :------- | :------ | -------: | ----------: |
| Species one | 1       | 5S       | p       |      0.5 |         0.5 |
| Species one | 1       | 45S      | q       |      1.0 |         0.5 |
| Species one | X       | 45S      | p       |      1.0 |         1.0 |
| Species one | 3       | DAPI     | q       |      1.0 |         1.0 |

For fixing column names use something
    like:

    colnames(mydfMarkColor)<-c("OTU", "chrName","markName","markArm","markSize","markDistCen")

## 4\. Special dataframe for centromeric marks’ data

Open or make your mark data as a dataframe. This dataframe has the marks
present in all karyotypes without position info.

``` r
# We will use column OTU because dataframe of chromosome size has it
mydfOfCenMarks<-read.table(text=
"             OTU chrName markName
1  \"Species one\"     1     DAPI
2  \"Species one\"     X      CMA"  ,  header=TRUE, stringsAsFactors=FALSE,fill=TRUE)

knitr::kable(mydfOfCenMarks)
```

| OTU         | chrName | markName |
| :---------- | :------ | :------- |
| Species one | 1       | DAPI     |
| Species one | X       | CMA      |

## 5\. Plotting

You can plot without marks (use only 1st dataframe), but we will use all
4 dataframes created:

``` r
# library(idiogramFISH)

plotIdiograms(mydfChrSize, mydfMarkColor, mydfOfMarks, mydfOfCenMarks,
              dotRoundCorr=2, chrWidth=4.5, chrSpacing = 4,
              karSpacing=1.6,
              indexIdTextSize=.7, OTUTextSize=.7,
              markLabelSize=.7,
              rulerPos=-1.9, ruler.tck=-0.02, rulerNumberPos=.5, rulerNumberSize=.7
)
```

![](plotIdiogramsVigMD_files/figure-gfm/example-1.png)<!-- -->

## 6\. Example with several species (OTUs)

### To illustrate this, we will load some dataframes from the package

``` r
data(bigdfOfChrSize, bigdfOfMarks, bigdfDataCen, dfMarkColor)

# Chromosome sizes
knitr::kable(bigdfOfChrSize)
```

| OTU       | chrName | shortArmSize | longArmSize |
| :-------- | :------ | -----------: | ----------: |
| Species 1 | 1       |          1.5 |         2.0 |
| Species 1 | 2       |          2.0 |         2.5 |
| Species 1 | 3       |          1.0 |         1.5 |
| Species 2 | 1       |          3.0 |         4.0 |
| Species 2 | 2       |          4.0 |         5.0 |
| Species 2 | 3       |          2.0 |         3.0 |
| Species 2 | X       |          1.0 |         2.0 |
| Species 2 | 4       |          3.0 |         4.0 |
| Species 3 | 1       |          3.2 |         4.0 |
| Species 3 | 2       |          4.5 |         5.0 |
| Species 3 | 3       |          2.0 |         3.0 |
| Species 3 | 4       |          1.5 |         2.0 |
| Species 3 | 5       |          4.8 |         6.0 |
| Species 3 | 6       |          6.1 |         7.0 |
| Species 4 | 1       |          1.5 |         2.0 |
| Species 4 | 2       |          2.0 |         2.5 |
| Species 4 | 3       |          1.0 |         1.5 |
| Species 5 | 1       |          3.0 |         4.0 |
| Species 5 | 2       |          4.0 |         5.0 |
| Species 5 | 3       |          2.0 |         3.0 |
| Species 5 | X       |          1.0 |         2.0 |
| Species 5 | 4       |          3.0 |         4.0 |
| Species 6 | 1       |          3.2 |         4.0 |
| Species 6 | 2       |          4.5 |         5.0 |
| Species 6 | 3       |          2.0 |         3.0 |
| Species 6 | 4       |          1.5 |         2.0 |
| Species 6 | 5       |          4.8 |         6.0 |
| Species 6 | 6       |          6.1 |         7.0 |

``` r
# Mark characteristics, does not require OTU
knitr::kable(dfMarkColor)
```

| markName | markColor | style  |
| :------- | :-------- | :----- |
| 5S       | red       | dots   |
| 45S      | green     | square |
| DAPI     | blue      | square |
| CMA      | yellow    | square |

``` r
# Mark position
knitr::kable(bigdfOfMarks)
```

| OTU       | chrName | markName | markArm | markDistCen | markSize |
| :-------- | :------ | :------- | :------ | ----------: | -------: |
| Species 1 | 1       | 5S       | p       |         0.5 |        1 |
| Species 1 | 1       | 45S      | q       |         0.5 |        1 |
| Species 1 | 2       | 45S      | p       |         1.0 |        1 |
| Species 1 | 3       | DAPI     | q       |         1.0 |        1 |
| Species 3 | 3       | 5S       | p       |         1.0 |        1 |
| Species 3 | 3       | DAPI     | q       |         1.0 |        1 |
| Species 3 | 4       | 45S      | p       |         2.0 |        1 |
| Species 3 | 4       | DAPI     | q       |         2.0 |        1 |
| Species 3 | 5       | CMA      | q       |         2.0 |        1 |
| Species 3 | 6       | 5S       | q       |         0.5 |        1 |

``` r
# Centromeric Marks only
knitr::kable(bigdfDataCen)
```

| OTU       | chrName | markName |
| :-------- | ------: | :------- |
| Species 2 |       1 | DAPI     |
| Species 2 |       4 | CMA      |

### Plotting

``` r
# library(idiogramFISH)

plotIdiograms(bigdfOfChrSize, dfMarkColor, bigdfOfMarks, bigdfDataCen,
                        karHeight=1.2,karSpacing=2.2,
                        dotRoundCorr = .5, distTextChr=.5,
                        indexIdTextSize=.7, OTUTextSize=.9,
                        legend="aside",markLabelSize=.7, markLabelSpacer=1,
                        morpho=F, xlimRightMod=2,
                        ruler=TRUE,rulerPos=-.9, rulerPosMod=3, ruler.tck=-0.004,
                        rulerNumberPos=.4, rulerNumberSize=.4,
                        )
```

![](plotIdiogramsVigMD_files/figure-gfm/example3-1.png)<!-- -->
